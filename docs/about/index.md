# About me

![](../images/avatar-photo.jpg)

Hi! I am Elias Rautio. I am an a mechanical engineering student, and I run a small business based on custom manufacturing with 3D printing and lasercutting.

Visit this website to see my work!

## My background

I have lived my whole life in Oulu, and I really like the city. I have been interested in making all kinds of stuff by myself since I was kid. I often spent several hours first drawing and scketching my ideas on paper, and then did them on backyard with my dads tools. Mechanical engineering was really obivious choice for me when it came to time to choose my studies. I am going to graduate in a year from now.

## Previous work

I was always really interested in 3D printing but I could not afford to buy one so built my first 3D printer five years ago. First my friends and relatives interested in it and started asking me to print them stuff. Later some companies asked if I could make them something also, and I decided to start a small business. Now I own four 3D printers, CO2 lasercutter, pad printing machine and some other equipment also. 

I have learned everything myself, and I decided to attend FabLab to see what else I could learn about fabrication.
